#include <iostream>
#include <WinSock2.h> //inclide para trabalhar com winsock

using namespace std;

#pragma comment(lib, "Ws2_32.lib") //add arquivo para trabalhar com winsock

int main(int argc, int *argv[]) {

	WSADATA wsa; //variavel para receber iniciacao do winsock

	cout << "\nIniciando Winsock..." << endl;

	if (WSAStartup(MAKEWORD(1, 1), &wsa) == SOCKET_ERROR) { //iniciar winsock
		cout << "Falha. Erro no codigo " << WSAGetLastError() << endl; //se nao iniciar
		return 1;
	}

	cout << "Inicializado." << endl;

	system("pause");

	return 0;
}