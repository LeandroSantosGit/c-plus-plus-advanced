#pragma once

namespace BancoDadosMysqlCadastro {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace MySql::Data::MySqlClient;
	
	

	/// <summary>
	/// Sum�rio para MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
		String^ nome;
		String^ sobrenome;
		int idade;
		int id;

	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Adicione o c�digo do construtor aqui
			//
		}

	protected:
		/// <summary>
		/// Limpar os recursos que est�o sendo usados.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	protected:
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;

	private:
		/// <summary>
		/// Vari�vel de designer necess�ria.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necess�rio para suporte ao Designer - n�o modifique 
		/// o conte�do deste m�todo com o editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->SuspendLayout();
			// 
			// dataGridView1
			// 
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Location = System::Drawing::Point(42, 33);
			this->dataGridView1->Margin = System::Windows::Forms::Padding(4);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->Size = System::Drawing::Size(591, 248);
			this->dataGridView1->TabIndex = 0;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Verdana", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(66, 308);
			this->label1->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(44, 16);
			this->label1->TabIndex = 1;
			this->label1->Text = L"Nome";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Verdana", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(66, 368);
			this->label2->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(81, 16);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Sobrenome";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Verdana", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label3->Location = System::Drawing::Point(437, 308);
			this->label3->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(45, 16);
			this->label3->TabIndex = 3;
			this->label3->Text = L"Idade";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(168, 308);
			this->textBox1->Margin = System::Windows::Forms::Padding(4);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(218, 22);
			this->textBox1->TabIndex = 4;
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(168, 368);
			this->textBox2->Margin = System::Windows::Forms::Padding(4);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(218, 22);
			this->textBox2->TabIndex = 5;
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(496, 308);
			this->textBox3->Margin = System::Windows::Forms::Padding(4);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(116, 22);
			this->textBox3->TabIndex = 6;
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->button1->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button1->Location = System::Drawing::Point(42, 452);
			this->button1->Margin = System::Windows::Forms::Padding(4);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(112, 28);
			this->button1->TabIndex = 7;
			this->button1->Text = L"INSERIR";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->button2->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button2->Location = System::Drawing::Point(200, 452);
			this->button2->Margin = System::Windows::Forms::Padding(4);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(112, 28);
			this->button2->TabIndex = 8;
			this->button2->Text = L"DELETAR";
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// button3
			// 
			this->button3->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->button3->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button3->Location = System::Drawing::Point(363, 452);
			this->button3->Margin = System::Windows::Forms::Padding(4);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(112, 28);
			this->button3->TabIndex = 9;
			this->button3->Text = L"ATUALIZAR";
			this->button3->UseVisualStyleBackColor = false;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
			// 
			// button4
			// 
			this->button4->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->button4->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button4->Location = System::Drawing::Point(521, 452);
			this->button4->Margin = System::Windows::Forms::Padding(4);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(112, 28);
			this->button4->TabIndex = 10;
			this->button4->Text = L"PESQUISAR";
			this->button4->UseVisualStyleBackColor = false;
			this->button4->Click += gcnew System::EventHandler(this, &MyForm::button4_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(7, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::GradientInactiveCaption;
			this->ClientSize = System::Drawing::Size(679, 511);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->dataGridView1);
			this->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->Margin = System::Windows::Forms::Padding(4);
			this->Name = L"MyForm";
			this->Text = L"Cadastro de Alunos";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: void atualiza() { //funcao atualiza os dados dentro dataGridView1

		String^ constring = "datasource=localhost;port=3307;username=root;password=1234"; //string de conexao
		MySqlConnection^ condatabase = gcnew MySqlConnection(constring); //conexao com banco
		MySqlCommand^ cmddatabase = gcnew MySqlCommand("SELECT * FROM cadastro.alunos;", condatabase); //comando para selecionar no banco
		MySqlDataReader^ myreader; //apresentar dados no dataGridView1
		DataTable^ dt = gcnew DataTable();

		condatabase->Open(); //abertura da conexao
		myreader = cmddatabase->ExecuteReader(); //coletar os dados do banco
		dataGridView1->DataSource = dt; //passar para dataGridView1
		dataGridView1->Refresh(); //atualizar dataGridView1
		dt->Load(myreader);

	} 

	private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {

		atualiza(); //aplicacao ja inicia mostrando dados
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

		nome = textBox1->Text;
		sobrenome = textBox2->Text;
		idade = Convert::ToInt32(textBox3->Text);

		String^ constring = "datasource=localhost;port=3307;username=root;password=1234";
		MySqlConnection^ condatabase = gcnew MySqlConnection(constring);
		MySqlCommand^ cmddatabase = gcnew MySqlCommand("INSERT INTO cadastro.alunos(nome, sobrenome, idade) VALUES('" + nome + "', '" + sobrenome + "', '" + idade + "');", condatabase);
		condatabase->Open();
		cmddatabase->ExecuteNonQuery();

		//limpar caixa de texto
		textBox1->Text = "";
		textBox2->Text = "";
		textBox3->Text = "";

		atualiza();
	}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {

	id = Convert::ToInt32(dataGridView1->CurrentRow->Cells[0]->Value); //pegar numero id

	String^ constring = "datasource=localhost;port=3307;username=root;password=1234";
	MySqlConnection^ condatabase = gcnew MySqlConnection(constring);
	MySqlCommand^ cmddatabase = gcnew MySqlCommand("DELETE FROM cadastro.alunos WHERE id = '" + id + "';", condatabase); //exclir aluno pelo id
	condatabase->Open();
	cmddatabase->ExecuteNonQuery();

	atualiza();
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {

	nome = textBox1->Text;
	sobrenome = textBox2->Text;
	idade = Convert::ToInt32(textBox3->Text);
	id = Convert::ToInt32(dataGridView1->CurrentRow->Cells[0]->Value);

	String^ constring = "datasource=localhost;port=3307;username=root;password=1234";
	MySqlConnection^ condatabase = gcnew MySqlConnection(constring);
	MySqlCommand^ cmddatabase = gcnew MySqlCommand("update cadastro.alunos set nome='" + nome + "', sobrenome='" + sobrenome + "',idade='" + idade + "' where id='" + id + "';", condatabase);
	condatabase->Open();
	cmddatabase->ExecuteNonQuery();

	textBox1->Text = "";
	textBox2->Text = "";
	textBox3->Text = "";

	atualiza();
}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {

	nome = textBox1->Text;
	sobrenome = textBox2->Text;
	//idade = Convert::ToInt32(textBox3->Text);

	String^ constring = "datasource=localhost;port=3307;username=root;password=1234";
	MySqlConnection^ condatabase = gcnew MySqlConnection(constring);
	MySqlCommand^ cmddatabase = gcnew MySqlCommand("select * from cadastro.alunos where nome like '" + nome + "%';", condatabase);
	MySqlDataReader^ myreader;
	DataTable^ dt = gcnew DataTable();

	condatabase->Open();
	myreader = cmddatabase->ExecuteReader();
	dataGridView1->DataSource = dt;
	dataGridView1->Refresh();
	dt->Load(myreader);
}
};
}
