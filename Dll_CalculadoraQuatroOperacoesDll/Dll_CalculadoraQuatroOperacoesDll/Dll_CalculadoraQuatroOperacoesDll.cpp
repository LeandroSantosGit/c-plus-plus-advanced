// Dll_CalculadoraQuatroOperacoesDll.cpp: define as funções exportadas para o aplicativo DLL.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

namespace arit {

	void operacoes::Soma(int x, int y) {
		cout << "Soma: " << x + y << endl;
	}

	void operacoes::Subtracao(int x, int y) {
		cout << "Subtracao: " << x - y << endl;
	}

	void operacoes::Divisao(int x, int y) {

		if (y != 0) {
			cout << "Divisao: " << x / y << endl;
		}
		else {
			cout << "Nao pode ser dividido por zero" << endl;
		}

	}

	void operacoes::Multiplicacao(int x, int y) {
		cout << "Multiplicacao: " << x * y << endl;
	}
}


