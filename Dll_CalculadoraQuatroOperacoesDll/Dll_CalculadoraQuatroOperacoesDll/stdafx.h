// stdafx.h : arquivo de inclusão para inclusões do sistema padrões,
// ou inclusões específicas de projeto que são utilizadas frequentemente, mas
// são modificadas raramente
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Remova itens raramente utilizados dos cabeçalhos Windows
// Arquivos de Cabeçalho Windows:
#include <windows.h>



// TODO: adicionar referências de cabeçalhos adicionais que seu programa necessita

namespace arit {

	class operacoes {
	public:
		static __declspec(dllexport) void Soma(int x, int y);
		static __declspec(dllexport) void Subtracao(int x, int y);
		static __declspec(dllexport) void Divisao(int x, int y);
		static __declspec(dllexport) void Multiplicacao(int x, int y);
	};
}
