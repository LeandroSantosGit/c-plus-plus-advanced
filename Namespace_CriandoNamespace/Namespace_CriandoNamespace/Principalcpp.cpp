#include <iostream>

using namespace std;

namespace Criando {

	void minhaFuncao() { //funcao para exibir msg

		cout << "Usando namespace Criando" << endl;
	}
}

int main() {

	using namespace Criando;
	minhaFuncao();

	system("pause");

	return 0;
}