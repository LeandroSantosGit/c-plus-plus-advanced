#pragma once

#include <stdlib.h>
#include <time.h>
#include <Windows.h>

namespace ProjetoFinalJogoBancoDados {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace MySql::Data::MySqlClient;

	/// <summary>
	/// Sum�rio para MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{

	private: String^ nome;
	private: Image^ Pedra = Image::FromFile("\Pedratw.png");
	private: Image^ Papel = Image::FromFile("\Papeltw.png");
	private: Image^ Tesoura = Image::FromFile("\Tesouratw.png");
	
	private: System::Windows::Forms::Button^ button5;
			 int pontuacao = 0;
			 int id;
			 int hpontos;
			 int vpontos;

	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Adicione o c�digo do construtor aqui
			//
		}

	protected:
		/// <summary>
		/// Limpar os recursos que est�o sendo usados.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	protected:
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::TextBox^  textBox1;
	//private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label9;

	private:
		/// <summary>
		/// Vari�vel de designer necess�ria.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necess�rio para suporte ao Designer - n�o modifique 
		/// o conte�do deste m�todo com o editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Verdana", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(43, 38);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(63, 16);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Pontos:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(112, 38);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(17, 18);
			this->label2->TabIndex = 1;
			this->label2->Text = L"0";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Verdana", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label3->Location = System::Drawing::Point(375, 38);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(63, 16);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Pontos:";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label4->Location = System::Drawing::Point(444, 38);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(17, 18);
			this->label4->TabIndex = 3;
			this->label4->Text = L"0";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Arial", 20.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label5->Location = System::Drawing::Point(281, 121);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(33, 32);
			this->label5->TabIndex = 4;
			this->label5->Text = L"X";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Font = (gcnew System::Drawing::Font(L"Verdana", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label6->Location = System::Drawing::Point(48, 193);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(81, 16);
			this->label6->TabIndex = 5;
			this->label6->Text = L"Seu Nome";
			this->label6->TextChanged += gcnew System::EventHandler(this, &MyForm::label6_TextChanged);
			// 
			// button1
			// 
			this->button1->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->button1->Enabled = false;
			this->button1->Font = (gcnew System::Drawing::Font(L"Verdana", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button1->Location = System::Drawing::Point(25, 223);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(81, 28);
			this->button1->TabIndex = 6;
			this->button1->Text = L"PEDRA";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->button2->Enabled = false;
			this->button2->Font = (gcnew System::Drawing::Font(L"Verdana", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button2->Location = System::Drawing::Point(115, 223);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(81, 28);
			this->button2->TabIndex = 7;
			this->button2->Text = L"PAPEL";
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// button3
			// 
			this->button3->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->button3->Enabled = false;
			this->button3->Font = (gcnew System::Drawing::Font(L"Verdana", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button3->Location = System::Drawing::Point(217, 223);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(81, 28);
			this->button3->TabIndex = 8;
			this->button3->Text = L"TESOURA";
			this->button3->UseVisualStyleBackColor = false;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
			// 
			// button4
			// 
			this->button4->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->button4->Font = (gcnew System::Drawing::Font(L"Verdana", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button4->Location = System::Drawing::Point(25, 270);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(273, 28);
			this->button4->TabIndex = 9;
			this->button4->Text = L"PARAR JOGO";
			this->button4->UseVisualStyleBackColor = false;
			this->button4->Click += gcnew System::EventHandler(this, &MyForm::button4_Click);
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Font = (gcnew System::Drawing::Font(L"Arial", 11.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label7->Location = System::Drawing::Point(22, 342);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(54, 18);
			this->label7->TabIndex = 10;
			this->label7->Text = L"NOME";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(82, 342);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(176, 20);
			this->textBox1->TabIndex = 11;
			// 
			// button5
			// 
			this->button5->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->button5->Font = (gcnew System::Drawing::Font(L"Verdana", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button5->Location = System::Drawing::Point(264, 334);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(99, 28);
			this->button5->TabIndex = 12;
			this->button5->Text = L"CADASTRAR";
			this->button5->UseVisualStyleBackColor = false;
			this->button5->Click += gcnew System::EventHandler(this, &MyForm::button5_Click);
			// 
			// pictureBox1
			// 
			this->pictureBox1->BackColor = System::Drawing::SystemColors::ControlLight;
			this->pictureBox1->Location = System::Drawing::Point(29, 71);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(218, 119);
			this->pictureBox1->TabIndex = 13;
			this->pictureBox1->TabStop = false;
			// 
			// pictureBox2
			// 
			this->pictureBox2->BackColor = System::Drawing::SystemColors::ControlLight;
			this->pictureBox2->Location = System::Drawing::Point(352, 71);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(218, 119);
			this->pictureBox2->TabIndex = 14;
			this->pictureBox2->TabStop = false;
			// 
			// dataGridView1
			// 
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Location = System::Drawing::Point(369, 205);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->Size = System::Drawing::Size(220, 157);
			this->dataGridView1->TabIndex = 15;
			this->dataGridView1->CellContentClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MyForm::dataGridView1_CellContentClick);
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Font = (gcnew System::Drawing::Font(L"Bell MT", 11.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label8->Location = System::Drawing::Point(174, 9);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(238, 19);
			this->label8->TabIndex = 16;
			this->label8->Text = L"JOGO PEDRA PAPEL TESOURA";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label9->Location = System::Drawing::Point(92, 314);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(125, 16);
			this->label9->TabIndex = 17;
			this->label9->Text = L"Cadastrar Jogador";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::ActiveCaption;
			this->ClientSize = System::Drawing::Size(601, 374);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->dataGridView1);
			this->Controls->Add(this->pictureBox2);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"MyForm";
			this->Text = L"JOGO ";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {

	String^ constring = "datasource=localhost;port=3307;username=root;password=1234";
	MySqlConnection^ condatabase = gcnew MySqlConnection(constring);
	MySqlCommand^ cmddatabase = gcnew MySqlCommand("SELECT * FROM cadastro.jogadores;", condatabase);
	MySqlDataReader^ myreader;
	DataTable^ dt = gcnew DataTable();

	condatabase->Open();
	myreader = cmddatabase->ExecuteReader();
	dataGridView1->DataSource = dt;
	dataGridView1->Refresh();
	dt->Load(myreader);
}
private: System::Void dataGridView1_CellContentClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {

	label6->Text = dataGridView1->CurrentRow->Cells[1]->Value->ToString();
}

private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

	this->pictureBox1->Image = this->Pedra;
	Inimigo();
	Compara();

	if (pictureBox1->Image == this->Pedra && pictureBox2->Image == this->Tesoura) {
		hpontos += 1;
	}
	else if (pictureBox1->Image == this->Pedra && pictureBox2->Image == this->Papel) {
		vpontos += 1;
	}

	this->label2->Text = hpontos.ToString();
	this->label4->Text = vpontos.ToString();

	GameOver();
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {

	this->pictureBox1->Image = this->Papel;
	Inimigo();
	Compara();

	if (pictureBox1->Image == this->Papel && pictureBox2->Image == this->Pedra) {
		hpontos += 1;
	}
	else if (pictureBox1->Image == this->Papel && pictureBox2->Image == this->Tesoura) {
		vpontos += 1;
	}

	this->label2->Text = hpontos.ToString();
	this->label4->Text = vpontos.ToString();

	GameOver();
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {

	this->pictureBox1->Image = this->Tesoura;
	Inimigo();
	Compara();

	if (pictureBox1->Image == this->Tesoura && pictureBox2->Image == this->Papel) {
		hpontos += 1;
	}
	else if (pictureBox1->Image == this->Tesoura && pictureBox2->Image == this->Pedra) {
		vpontos += 1;
	}

	this->label2->Text = hpontos.ToString();
	this->label4->Text = vpontos.ToString();

	GameOver();
}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {

	id = Convert::ToInt32(dataGridView1->CurrentRow->Cells[0]->Value);
	pontuacao = Convert::ToInt32(label2->Text);

	String^ constring = "datasource=localhost;port=3307;username=root;password=1234";
	MySqlConnection^ condatabase = gcnew MySqlConnection(constring);
	MySqlCommand^ cmddatabase = gcnew MySqlCommand("update cadastro.jogadores set pontos = '" + pontuacao + "' where id = '" + id + "';", condatabase);
	condatabase->Open();
	cmddatabase->ExecuteNonQuery();

	atualiza();
}
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {

	nome = textBox1->Text;

	String^ constring = "datasource=localhost;port=3307;username=root;password=1234";
	MySqlConnection^ condatabase = gcnew MySqlConnection(constring);
	MySqlCommand^ cmddatabase = gcnew MySqlCommand("insert into cadastro.jogadores(nome, pontos) values('" + nome + "', '" + pontuacao + "');", condatabase);
	condatabase->Open();
	cmddatabase->ExecuteNonQuery();

	textBox1->Text = "";

	atualiza();
}

private: System::Void label6_TextChanged(System::Object^  sender, System::EventArgs^  e) {

	button1->Enabled = true;
	button2->Enabled = true;
	button3->Enabled = true;
}

	
		 //funcoes para auxiliares
		private: void atualiza() { //funcao atualiza os dados dentro dataGridView1

			String^ constring = "datasource=localhost;port=3307;username=root;password=1234";
			MySqlConnection^ condatabase = gcnew MySqlConnection(constring);
			MySqlCommand^ cmddatabase = gcnew MySqlCommand("select * from cadastro.jogadores;", condatabase);
			MySqlDataReader^ myreader;
			DataTable^ dt = gcnew DataTable();

			condatabase->Open();
			myreader = cmddatabase->ExecuteReader();
			dataGridView1->DataSource = dt;
			dataGridView1->Refresh();
			dt->Load(myreader);
		}

		private: void Inimigo() { //inteligencia do jogador computador

			srand(time(NULL));
			int numero;
			numero = rand() % 3; //funcao randomica

			if (numero == 0) { //determinando image para cada numero random
				this->pictureBox2->Image = this->Pedra;
			}
			else if (numero == 1) {
				this->pictureBox2->Image = this->Papel;
			}
			else if (numero == 2) {
				this->pictureBox2->Image = this->Tesoura;
			}
		}

		private: void Compara() {

			if (this->pictureBox1->Image == this->Pedra && pictureBox2->Image == this->Pedra) {
				MessageBox::Show("Duas Pedras", "Empate", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
			}
			else if (this->pictureBox1->Image == this->Papel && pictureBox2->Image == this->Papel) {
				MessageBox::Show("Dois Papel", "Empate", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
			}
			else if (this->pictureBox1->Image == this->Tesoura && pictureBox2->Image == this->Tesoura) {
				MessageBox::Show("Duas Tesouras", "Empate", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
			}
		}

		private: void GameOver() { //verificar se jogador ou computado chegou a 10 pontos e finalizamos e infomar vencedor

			if (hpontos == 10) {
				MessageBox::Show("Voce Venceu", "Vitoria", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
				hpontos;
				vpontos;
				this->label2->Text = hpontos.ToString();
				this->label4->Text = vpontos.ToString();

				button1->Enabled = false;
				button2->Enabled = false;
				button3->Enabled = false;
			}

			if (vpontos == 10) {
				MessageBox::Show("Voce Perdeu", "Derrota", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
				hpontos;
				vpontos;
				this->label2->Text = hpontos.ToString();
				this->label4->Text = vpontos.ToString();

				button1->Enabled = false;
				button2->Enabled = false;
				button3->Enabled = false;
			}
		}

};
}
