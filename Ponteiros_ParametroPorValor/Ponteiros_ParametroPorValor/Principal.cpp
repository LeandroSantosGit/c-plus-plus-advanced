#include <iostream>

using namespace std;

typedef int* IntP; //definimos um ponteiro inteiro
void engana(IntP temp); //prototipo funcao 

int main() {

	IntP p;

	p = new int; //variavel denamica e local de memoria
	*p = 50; //valor armazanado na memoria

	cout << "Depois de chamar a funcao engana, *p = " << *p << endl;

	engana(p); //chamando funcao

	cout << "Depois de chamar a funcao engana, *p = " << *p << endl;

	system("pause");

	return 0;
}

void engana(IntP temp) { //funcao com argumento

	*temp = 100;

	cout << "Dentro da funcao *p = " << *temp << endl;

}

/*
O programa da a entender que o valor de p mudou, mas ele nao mudou. A funcao engana altera apenas
o valor da variavel apontada por p, mas nao alterou o valor de p em sim. 
*/