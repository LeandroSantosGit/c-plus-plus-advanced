#include <iostream>

using namespace std;

int Soma();
int Subtracao();

int main() {

	cout << "Soma: " << Soma() << endl;
	cout << "Subtracao: " << Subtracao() << endl;

	system("pause");

	return 0;
}

int Soma() {

	__asm {

		mov eax, 100 //mover valor para eax
		mov ebx, 50 //mover valor para ebx
		add eax, ebx //soma dos valores
	}
}

int Subtracao() {

	__asm {

		mov eax, 100 //mover valor para eax
		mov ebx, 50 //mover valor para ebx
		sub eax, ebx //subtracao dos valores
	}
}