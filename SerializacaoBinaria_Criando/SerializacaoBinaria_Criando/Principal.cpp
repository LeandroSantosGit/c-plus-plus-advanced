#include <fstream>
#include <iostream>
#include <cstring>

using namespace std;

class Pessoa { //classe de arquivos 

	public:
		char nome[40];
		char sobrenome[120];
		char genero;
		int idade;
};

int main() {

	Pessoa um; //instancia

	//passar dados
	strcpy_s(um.nome, "Leandro");
	strcpy_s(um.sobrenome, "Santos");
	um.genero = 'M';
	um.idade = 50;

	ofstream ofs("Teste.txt", ios::binary); //enviar informarcoes para arquivo
	ofs.write((char*)&um, sizeof(um)); //escrever no arquivo

	return 0;
}