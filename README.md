## C++ Avan�ado

No curso foi abordado Como criar, exportar e importar DLLs; adicionar c�digos Assembly junto com o C++; criar Sockets; implementar streams.

* Strings - strcpy() ou strcpy_s(); strncpy() ou strncpy_s(); strcat() ou strcat_s(); Fun��es putback e peek e ignore
* Ponteiros
* Vetores din�micos
* DLL
* Assembly
* Sockets
* Namespaces
* Streams de E/S
* Serializa��o bin�ria
* Banco de dados - MySQL
* GCI
* Ogre