#include <iostream>

using namespace std;

int main() {

	int *p1, *p2; //ponteiros irao apontar para algum lugar

	p1 = new int; //p1 aponta para variavel dinamica

	*p1 = 50; //definir valor que sera guardado em um local na memoria
	p2 = p1; //p2 aponta para mesmo local de memoria que p1

	cout << "*p1: " << *p1 << endl; //exibir valores
	cout << "*p2: " << *p2 << "\n\n";

	*p2 = 80; //novo valor, sendo assim p1 e p2 iram exibir o novo valor por estarem apontando para mesmo local de memoria

	cout << "*p1: " << *p1 << endl; //exibir novo valores
	cout << "*p2: " << *p2 << "\n\n";

	p1 = new int; //p1 aponta para nova variavel dinamica

	*p1 = 100; //novo valor e novo local de memoria 

	cout << "*p1: " << *p1 << endl;
	cout << "*p2: " << *p2 << "\n\n";

	system("pause");

	return 0;
}