#include <iostream>

using namespace std;

int main() {

	int *ponteiro1, *ponteiro2;

	ponteiro1 = new int; //variavel dinamica e local de memoria

	*ponteiro1 = 10; //valor
	ponteiro2 = ponteiro1; //ponteiro2 aponta para mesmo local de memoria que ponteiro1

	cout << "Ponteiro1: " << *ponteiro1 << endl;
	cout << "Ponteiro2: " << *ponteiro2 << endl;

	ponteiro1 = new int; //novo local de memoria

	*ponteiro1 = 1000; //novo valor

	cout << "Ponteiro1: " << *ponteiro1 << endl;
	cout << "Ponteiro2: " << *ponteiro2 << endl;

	system("pause");

	return 0;

}