#include <iostream>

using namespace std;

namespace Soma {

	void funcaoCalcula(int numero1, int numero2) {

		cout << "Soma: " << numero1 + numero2 << endl;
	}
}

namespace Subtracao {

	void funcaoCalcula(int numero1, int numero2) {

		cout << "Subtracao: " << numero1 - numero2 << endl;
	}
}

int main() {

	Soma::funcaoCalcula(10, 5);
	Subtracao::funcaoCalcula(10, 5);

	system("pause");

	return 0;
}