#include <iostream>
#include <cstring>

using namespace std;

int main() {

	char c1 = toupper('m'); //toupper ira fazer que caracter minusculo seja maiusculo

	cout << "c1: " << c1 << endl;
	///////////////////////////////////
	char c2 = tolower('M'); //tolower ira fazer que caracter maiusculo seja minusculo

	cout << "c2: " << c2 << endl;
	///////////////////////////////////
	char c3 = 'M';

	if (isupper(c3)) { //verificar se � mauisculo
		cout << "c3: Maiuscula" << endl;
	}
	////////////////////////////////////
	char c4 = 'm';

	if (islower(c4)) { //verificar se � minusculo
		cout << "c4: Minuscula" << endl;
	}
	////////////////////////////////////
	char c5 = 'm';

	if (isalpha(c5)) { //verificar se � letra
		cout << "c5: Uma Letra" << endl;
	}
	////////////////////////////////////
	char c6 = '3';

	if (isdigit(c6)) { //verificar se � numero
		cout << "c6: Um numero" << endl;
	}

	system("pause");

	return 0;
}