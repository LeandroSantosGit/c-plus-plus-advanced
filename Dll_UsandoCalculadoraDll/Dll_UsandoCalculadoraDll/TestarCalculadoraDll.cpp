#include <iostream>
#include "stdafx.h"

using namespace std;
using namespace arit;

int main() {

	int v1, v2;

	v1 = 10; //passando valores
	v2 = 4;

	//funcoes da Dll
	operacoes::Soma(v1, v2);
	operacoes::Subtracao(v1, v2);
	operacoes::Divisao(v1, v2);
	operacoes::Multiplicacao(v1, v2);

	system("pause");

	return 0;
}