#include <iostream>
#include <fstream> //biblioteca para usar streams

using namespace std;

using std::ifstream; //ler informacoes
using std::ofstream; //enviar informacoes

int main() {

	ifstream in;
	ofstream out;

	//abrir arquivos
	in.open("Ler.txt");
	out.open("Escrever.txt");

	int x, y, z; //variaveis para representar numeros do Ler.txt
	in >> x >> y >> z; //efetuar aleitura do arquivo Ler.txt
	out << "Soma dos valores do arquivo Ler.txt e: " << (x + y + z); //escrever no arquivo Escrever.txt a soma

	in.close();
	out.close();

	cout << "Soma escrita no arquivo Escrever.txt" << endl;

	system("pause");

	return 0;
}