#include <iostream>

using namespace std;

typedef int* IntPtr; //ponteiro

void preenchimento(int x[], int tamanho); //prototipo da funcao

int main() {

	int arrayTam; //sera usada para definir o tamanho do vetor

	cout << "Quantos numeros colocamos na lista: "; //aqui o usuario imforma o tamanho do vetor
	cin >> arrayTam;

	IntPtr num;
	num = new int[arrayTam]; //criamos vetor dinamico

	preenchimento(num, arrayTam); 

	system("pause");

	return 0;
}

void preenchimento(int x[], int tamanho) { //primeiro argumento e o valor adcionado no vetor
	                                       //o segundo e o tamanho do vetor
	cout << "\n" << "Digite " << tamanho << " numeros inteiros" << endl;

	for (int i = 0; i < tamanho; i++) { //preencher vetor
		cout << "Digite: ";
		cin >> x[i];
	}

	cout << "\n\n" << "Numeros digitados" << endl;
	for (int i = 0; i < tamanho; i++) { //exibir valores adcionados
		cout << x[i] << endl;
	}
}

/*
vetor dinamico o seu tamanho � definido no momento de execucao, fazendo com que o tamanho seja 
realmente oque precisamos no momento de execucao.

*/