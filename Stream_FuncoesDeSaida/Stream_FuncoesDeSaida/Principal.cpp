#include <iostream>
#include <fstream>

using namespace std;

int main() {

	ofstream out;
	out.open("Arquivo.txt"); //abrir arquivo
	out.setf(ios::showpos | ios::fixed); //escrever numeros positivos 
	out.precision(3); //com 3 casas apos a virgula

	float numero1, numero2;
	numero1 = 10.5;
	numero2 = 5.05;

	out << numero1 << " " << numero2 << endl;

	system("pause");

	return 0;

}

/*
ios::showpos - sinal positivo na frente dos numeros
ios::fixed - numeros flutuantes
*/