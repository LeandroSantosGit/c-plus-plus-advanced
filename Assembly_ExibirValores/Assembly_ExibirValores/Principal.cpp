#include <iostream>

using namespace std;

int PegaASM() {

	__asm {
		mov eax, 100 //mover o valor para eax
	}
}

int main() {

	cout << PegaASM() << endl;

	system("pause");

	return 0;
}