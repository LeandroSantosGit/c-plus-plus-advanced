#include <iostream>
#include <cstring>

using namespace std;

int main() {

	char nome[] = "Leandro";

	cout << "Nome:" << nome << endl;

	//funcao de copiar

	strcpy_s(nome, "Pereira"); //ira copiar esse novo nome

	cout << "Nome copiado: " << nome;

	getchar();

	return 0;
}