#include <iostream>

using namespace std;

int main() {

	char first, last; //variaveis para armazenar nome e sobrenome

	cout << "Escreva seu nome e sobrenome: ";

	first = cin.get();
	std::cin.ignore(200, '\n'); //passamos o numero de caracteres a serem ignorados ate o usuario digitar espaco

	last = cin.get();

	std::cout << "Suas iniciais sao: " << first << last << "\n";

	system("pause");

	return 0;
}

//ignore funcao ira ignorar caracteres