#include <iostream>

using namespace std;

namespace Exibir1 {

	void minhaFuncao() {

		cout << "Usando o namespace Exibir1" << endl;
	}
}

namespace Exibir2 {

	void minhaFuncao() {

		cout << "Usando o namespace Exibir2" << endl;
	}
}

int main() {

	//forma certa
	Exibir1::minhaFuncao();
	Exibir2::minhaFuncao();


	/* 
	Esta forma dera conflito de ambiguidade

	using namespace Exibir1;
	minhaFuncao();
	using namespace Exibir2;
	minhaFuncao();
	*/ 

	system("pause");

	return 0;
}