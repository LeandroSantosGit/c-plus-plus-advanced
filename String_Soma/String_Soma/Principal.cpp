#include <iostream>
#include <string>

using namespace std;

int main() {

	string numero1 = "10";
	string numero2 = "9.5";
	int numero3 = 5;
	float numero4 = 3.5;


	int auxint = stoi(numero1); //converter string para inteiro

	float auxfloat = stof(numero2);

	cout << "Soma int: " << auxint + numero3 << endl;

	cout << "Soma float: " << auxfloat + numero4 << endl;

	system("pause");

	return 0;

}