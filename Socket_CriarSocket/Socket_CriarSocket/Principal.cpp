#include <iostream>
#include <WinSock2.h>

using namespace std;

#pragma comment(lib, "Ws2_32.lib")

int main(int arcg, int argv[]) {

	WSADATA wsa;
	SOCKET s;

	cout << "\nIniciando Winsock..." << endl;

	if (WSAStartup(MAKEWORD(1, 1), &wsa) == SOCKET_ERROR) {

		cout << "Falha. Erro no codigo." << WSAGetLastError() << endl;
		return 1;
	}

	cout << "Inicializado." << endl;

	if ((s = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET) { //criando socket

		cout << "Socket n�o foi criado: " << WSAGetLastError() << endl;
	}

	cout << "Socket criado." << endl;

	system("pause");

	return 0;
}

/*
AF_INET - protocolo de intenet 4 IPv4
SOCK_STREAM - protocolo TCP
*/