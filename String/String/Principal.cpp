#include <iostream>

using namespace std;

int main() {

	char nome1[6] = "Marco"; //vetor de 6 posicoes, se nome for maior ira dar erro overflow
	char nome2[1000] = "Marcos"; //com posicoes exagerada deixando aplicativo lento
	char nome3[] = "Marcos";//posicoes nao declarada, aplicacao define as posicoes com forme a variavel 

	cout << nome1 << endl;
	cout << nome2 << endl;
	cout << nome3;
		
	getchar(); //exibir e nao fechar aplicacao

	return 0;
}