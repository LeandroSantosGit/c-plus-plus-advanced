#include <iostream>
#include <cstring>

using namespace std;

int main() {

	char nome[] = "Leandro";

	cout << "Nome: " << nome << endl;

	//funcao de copiar por posicao

	strncpy_s(nome, "Pereira", 2); //copiar a quantidade de posicoes definidas 

	cout << "Nome copiado: " << nome;

	getchar();

	return 0;
}